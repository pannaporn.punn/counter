/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pannaporn.counter;

/**
 *
 * @author akemi
 */
public class Main {
    public static void main(String[] args) {
       // Reference Type
       // Class
        Counter counter1 = new Counter();
        Counter counter2 = new Counter();
        Counter counter3 = new Counter();
        counter1.increase();
        counter1.increase();
        counter1.increase();
        counter1.print();
        counter2.print();
        counter2.increase();
        counter2.print();
        counter2.decrease();
        counter2.print();
        counter1.increase();
        counter3.decrease();
        System.out.println("Print All counter");
        counter1.print();
        counter2.print();
        counter3.print();
    }
}
